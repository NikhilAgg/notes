# Materials
## Measuring Young's Modulus
There are 4 main ways of finding young's modulus:
- Tensile testing
- Bending stiffness of a beam
- Natural Frequency of vibrations
- Speed of sound in the material

### Tensile Testing
This is where you apply a tensile load to a material of uniform cross section and measure the strain for different stresses

![](.\images\test.svg)

Accuracy considerations
- elastic strains are small so measurements will have larger percentage errors
- If measuring strain with a machine, the machine must take into the account the flexing of the machine
- using a strain gauge is much more precise than a ruler

### Bending stiffness of a beam
A beam is loaded in three points to cause bending as below: 
![](.\images\bending.svg)

The stiffness of the beam is given by $\frac{F}{\delta}$ and $\frac{48EI}{L^{3}}$ and so $E$ can be found

Accuracy considerations:
- The beam deflects more in this case giving a smaller percentage error than the tensile testing
- $E$ depends more on the dimensions of the beam so we need to be more accurate when recording the dimensions
  
### Natural frequency of vibration
The natural frequency of a material depends on $EI$ and so $E$ can be calculated from it

Accuracy Considerations:
- Measuring frequency is more accurate than measuring deflection
- $E$ is once again very sensitive to the dimensions of the material thorugh $I$ and so dimensions need to be accurate

### Speed of sound in a material
Both the speed of sound in a material and young's modulus depend on the bond stiffness of the material and so using dimensional analysis we can find that the speed of sound in a material equals:

<br/>

$$\Large v = \sqrt{\frac{E}{\rho}}$$

<br/>

To find the speed we tap on end of a bar and time how long it takes for the longitudinal wave to get to the other side

Accuracy considerations:
- The time is very small and so we must use precise time measuring techniques which isn't too difficult
## Materials in 3D
### Poisson's Ratio
When an object is under uniaxial tension the object not only gets longer but also thinner. This is not due to volume being conserved (as volume isn't conserved as the space between the atoms may increase), but due to the way the atomic bonds deform.

For a given material the strain due to getting thinner in the two perpendicular directions because of the uniaxial tension is proportional to that tension. We call the proportionality constant Poisson's ratio $\nu$

<br/>

$$\Large \nu = \frac{-Lateral \ Strain}{Tensile \ Strain}$$

<br/>

- The negative is to make the ratio is positive because $\nu$ is normally opposite to the tensile strain
### Stress in 3D
To find out how stress deforms an object in 3D consider a unit cube with 3 tensile forces on it as below:
![](.\images\cube.svg) 
We can analyse this problem by considering each force seperately, finding the corresponding strain and then using superposition to find the total strain.

So first looking at $\sigma_{1}$, $\sigma_1$ will cause a strain of $\frac{\sigma_{1}}{E}$ in the $1$ direction. In the other directions it will cause a strain of $-\frac{\nu \sigma_{1}}{E}$ due to poissons ratio. So we have that:

<br/>

$$\Large \epsilon_{1} = \frac{\sigma_{1}}{E}, \ \epsilon_{2} = \frac{\nu \sigma_{1}}{E}, \ \epsilon_{3} = \frac{\nu \sigma_{1}}{E} $$

<br/>

You can then do the same thing for the other forces. Then to find the total strain in a direction just add up all the strains in that direction.

### Dilation
As the volume of the object changes under load we can define a volumetric strain:
$$\Large \Delta = \frac{\Delta V}{V_{0}}$$

We call this strain dilation.
### Bulk modulus
Since we have a measure of volumetric strain, it makes sense to have a modulus to got along with that (modulus means to relate stress and strain together). However, in this case stress is different in different directions and so we can only create the modulus for when all the stresses are equal which is called hydrostatic stress.

Bulk modulus $k$ is:
$$\Large k = \frac{Hydrostatic \ stress}{Dilation} = \frac{E}{3(1-2\nu)}$$

## Shear stresses and strains
The strain mentioned before this has all been perpendicular to the material, however there is a type of stress that acts parallel to the material - this is called shear stress pictured below:

![](.\images\shear.svg) 

- Notice the shear on opposite faces acts in the opposite direction to ensure equilibrium is met

Unlike normal stresses, shear stress only deforms the shape of a material and cannot change the dimensions of the material like normal stress can.

We can reveal the shear stress by cutting through a material at an angle or applying a force that is not perpendicular to the cross section. As you can see below, as the two stresses are perpendicular to one another so they are components of the total resultant stress acting on an area.
- Remember you can't compare stress directly to force you must multipy it by area before.
- Also note the stress is a resultant force on the area - think of stress as presssure but it's not uniform
![](.\images\components.svg)

## Shear Strain and Modulus
### Shear Strain
Since we have shear stress we should also define shear strain $\gamma$. As shear only changes the shape we can't use the change in dimension so insteam we can use the angle the object is deformed through as picutred below:

![](.\images\shearStrain.svg)

Using small angle approximations we can see this angle is $\gamma = \frac{w}{l_{0}}$ 

### Shear Modulus   
Finally since we have both shear stress and strain, we can define a shear modulus $G$:
$$\Large G = \frac{\tau}{\gamma} = \frac{E}{2(1+\nu)}$$
- The second result is derived by comparing shear stress to uniaxial stress by slicing an object