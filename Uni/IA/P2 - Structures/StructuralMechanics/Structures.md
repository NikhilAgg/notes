# Structures
## Basics
- Start thinking about forces as vectors as the direction of a vector by itself can tell lots of information
  - This can be done by drawing force polygons or extending force lines and using equilbrium facts e.g if you have three forces they must all intersect at one point in order for moments to balance
- You can move forces to different parts of a structure by adding a couple of forces and an opposing moment. Moving the force to specific positions may simplify your working

## Pressure
### Constant Pressure
- We will only analyse 1D objects or 2D objects with a constant width
  - In the 2D cases, we can treat it as a 1D if we use pressure per width
  - Depth is irrelevant because the pressure only touches the surface in these cases.
- Pressure acts normally to a surface. 
- If we model the pressure as constant throughout the fluid then we can find the total pressure acting on a surface using $F = p\int{N(h)}{dh}$
  - where $p$ is the pressure (per unit width)
  - $N(h)$ is the perpendicular unit vector to the surface at height h
![](.\images\pressureComp.svg)
- However the intergral is complex and so an easier way of finding the total force is to split the pressure force into components where the force acting in each direction is $p \times$ the projected area e.g in the above example the horizontal component of pressure on the structure is $py$.

### Non-Constant Pressure
If we don't make the assumption the pressure is uniform throughout the fluid:

#### Hydrostatic Pressure
![](.\images\hydroPressure.svg)

The pressure is such that it supports the weight of the fluid above it. The weight of the fluid above a point in the container is $\frac{Mgh}{V} = \rho gh$. Once again this pressure acts in all directions

#### Finding resultant force
Because the pressure is changing now in order to find the total force we must include the pressure into our integral - $F \int{\rho ghN(h)dh}$

However, this is complex so we will only focus on straight surfaces so we can ignore the $N(h)$. 

![](.\images\pressureTrig.svg)

The pressure increases linearly with height and so we can easily integrate the pressure by finding the area of the trapezium above.
    - However other pressure functions may have to be integrated normally

#### Countering the moment
As the force isn't constant, it causes a resultant moment. To make our lives easier we can avoid dealing with this moment if we place the resultant force at a height which cancels out the moment.

We can find the height by integrating to find the moment then dividing by the total height - $h* = \frac{1}{h}{\int{p(h)hdg}}$.

Once again there is an easier way to do this, as we already know that the centroid of a triangle is $\frac{1}{3}$ the perp distance from the point to the opposite edge. We can split the pressure into a rectangle and triangle placing the force from the rectangle in the middle and the force from the triangle at the centroid of the trianlge.

# Friction
## Constant Friction
1. There are two types of friction - static friction (the value when the object isn't moving) and dynamic friction (the value when the object is moving). Static friction is normally higher than dynamic friction so each surface boundary will also have two coefficents of friction where $\mu_{s} > \mu _{d}$
2. In the theme of thinking of forces as vectors we can combine the reaction and friction forces to make one force. As $F/R = \mu$ this force will act at a specific angle for each surface boundary $\psi_{s} > \psi_{d}$ - one for each type of friction
![](.\images\angleFriction.svg)

## Non-constant Friction
Friction isn't always constant. In the cases we will consider this is because the reaction force isn't constant through an object.

In these cases you need to consider a small part of the object then integrate over it.
![](.\images\tensionFriction.svg)

In the example above, there are three variables - $N, d\theta, T/dT$. We want to find how $T$ varies with $\theta$ so we need to eliminate $N$. We have two equations by resolving horizontally and vertically. Once you have an equation in terms of $d\theta, T/dT$ you can solve the differential equation.

# Supports and Joints
## Basics
All joints constain the motion of the members connected to it. In the 2D structures we are considering, there are 3 degrees of motion - horizontal, vertical and rotation.

## Types of Joints
There are three types of joint:
![](.\images\joint.svg)
1. Pin-joints constraint vertical and horizontal movements but allow rotation and so reactions at pin joint are one force which can act in any direction.
2. Roller Supports only constrain motion in one direction and so reactions at roller supports are one force that acts in the same line it constrains
    - You can figure out which direction is constrained as it is perpendicular to the direction the support can roll.
3. Encastre Joints constrain all 3 degrees of freedom and so reactions include a force which can act in any direction and a moment.
    - We don't deal with reactions in this case but use bending moments and shear forces instead.

## Statical Detirminancy
Now we know that each joint constrains motion, when designing a structure we should make sure it is constrained in all 3 degrees of freedom.

![](.\images\statDetirmin.svg)

There are three cases:
1. You structure can be perfectly constrained and therefore member forces can be solved using equilibrium arguements - statically detirminate
2. Your structure can be over constrained and therefore your member forces depend on the material propeties of the structure and so can't be solved by equiibrium - statically indetirminate
3. Your structure is underdefined and so will fall over with no load.

Generally to make a statically detirminate structure you should make your structure out of triangles where each vertex is a pin joint.

### Euler's formula
There is a formula for figuring out if a structure is indetirminate. $Dj - b - r$
    - $D$ is the dimension you are working in
    - $j$ is the number of joints
    - $b$ is the number of bar (members)
    - $r$ is the number of restraints - the number of degrees of freedoms restricted by supports connected to the ground (can be more than 3)

- If $=0$ then statically detirminate
- If $<0$ then statically indetirminate
- If $>0$ then mechanism

This formula doesn't always get things right.

# Analysing member forces
## Pin jointed structures
### Techniques
![](.\images\structure.svg)
There are two main methods to analyse internal forces
1. Method of sections - imagine cutting out a peice of the structure and then add on the member forces of any members you cut through and external forces (reactions, loads, etc). Then solve equilibrium on your cut out to find the member forces
![](.\images\sections.svg)

2. Method of joints - go from joint to joint solving the equilbrium at each joint doing which ever joints you can until you can solve for the member you want.
    - To solve for member forces at a joint you can only have two unknown forces at the joint which can be solved by resolving up and down
![](.\images\joints.svg)


### Tips
1. You often need to find the external reactions by taking moments about one of them and resolving up and down
2. If you have three unknown forces, take moments about the intersection of two of them to solve for the third - also works for method of sections
3. Sections is quicker for finding one or two forces but joints is better for lots of forces
4. The following joint situations can help solve questions way quicker.
![](.\images\tricks.svg)

### Superposition
For the same structure under two different set of loads the member forces would be equal to adding the forces when each set is applied seperately
![](.\images\superposition.svg)

This can be useful when you have already calculated the member forces under the seperate loads or when you can calculate a more difficult load from simpler ones as above

## Shear forces and Bending moments
Not all structures can be pin jointed with straight members, and in such cases members will not just have axial tension forces but shear forces and bending moments. Examples of when a member will have shear forces and bending moments are below:
![](.\images\shearEx.svg)
To solve for these forces you will need to use method of sections to cut through the member and reveal the forces then solve as normal.

In order to get the signs right draw the forces exactly the same as in the diagram below:
![](.\images\shearSign.svg)

## Arches
### Segmented Arches
Segmented arches are arches made from trapzoidal blocks as pictured below:

We make a series of assumptions about these arches:
   - The blocks are infinitly strong
   - There are no bending moments at the block interfaces
   - There is infinite friction between the blocks

To figure out whether these arches will topple:
1. Imagine applying the load in question and marking on the points which will act as as hinge - points that would stay still under load
2. If you can draw a line connecting any two hinges which goes outside the lines of the arch it will topple, if not it will not.

![](.\images\segArch.svg)

This arch would fall as the thrust line (the dashed line) goes outside the arch

### Continous Arches
To tackle a continous arch, its best to split it according to the pin joints.
![](.\images\contArch.svg)

1. In split 1 as there are only two forces they must be colinear and so both act along the dotted line
2. In split 2 as there are three force they must all meet at one point. We figured out the reaction at the pin's direction is split 1 so we can find the direction of the reaction at the bottom left

You can then solve for the reactions by taking moments

## Stress
When acted on by an external load any body will act as a spring and provide a countering force.

To normalise this force for members of any dimensions we use stress where stress is $\sigma = \frac{F}{A}$ 

There are three types of stress:
![](.\images\stresses.svg)

To solve for horizontal and vertical stress all you need to do is to cut through an object to reveal their stress and resolve in the correct direction
   - Assume stress is constant through a cross sectional face
   - Remember to multiply the stress by area to get force or vice versa

## Structures
There are two main types of way to support a beam
- Simply supported beam is supported by a roller support and a pin support
- A cantilever is supported by an encastre joint at one end only.

If a beam is curved it must have bending moments. As bending moments are internal forces we must cut through the beam using method of sections to reveal them.

Beam assumptions:
- The beam won't break
- It doesn't deflect/bend alot
- Its 2D

A beam can be split into two different beams with different loading, which is useful if you have already analysed one of the loading situations.

Distributed forces at a point is equal to its area.
Split distributed loads and normal loads up and consider seperately
Line up $x=0$ with the origin of the equation of the distributed load.

If we consider a small peice of a beam $x$ under a vertical distributed load $p$ and draw on the internal forces on both sides and resolve we will get:
$$\Large p = \frac{dS}{dx}, \  S = \frac{dM}{dx}$$
- Makes sense as $S$ will balance out the total vertical force which is what the integral says
- Also if $S$ is the vertical force at each point integrating with $x$ will give $M$
- Only true for our sign convention.
- Solving differential equations so will need sutiable boundary conditions

Only works for distributed loads so will have to split concentrated and distributed loads by considering them seperatly or splitting the bar to seperate them (in the second case use boundary conditions that ensure continuity).