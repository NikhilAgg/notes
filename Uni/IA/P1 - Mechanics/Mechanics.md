# Mechanics
## Satelite Motion
- Conservation of angular momentum can be used as all forces acted centrally e.g no moments act on the body
- Conservation of energy could be used as gravity is the only force acting on the object and gravity is conservative
- The equation of motion can be solved using the substitution $u = \frac{1}{r}$. This works as when you differentiate $\frac{1}{u}$ you get $r^2$ and using the chain rule you can get $\dot \theta$ which together gives you the angular momentum of the body. As angular momentum is conserved so it acts as a constant when differentiating.
- The general equation for satelite motion is $\frac{1}{r} = \frac{GMm}{h}(1 + ecos(\theta))$ - Find e by subbing in a point
  - If $e = 0$, its circular motion
  - If $e < 1$, its an ellipse 
  - If $e = 1$, its an parabola       
  - If $e > 1$, its an hyperbola
- Use ellipse rules to find the axis lengths or anything else

## Energy
- Energy is the displacement persepective of Newton's Second Law with $E = \int{F dx}$
  - You can think of energy as how hard it is to move through a vector field
- $T + V = k$ is the conservation of energy and only holds true when all the forces acting on an object come from conservative force fields
  - A conservative force field is a field where work done is independant of route taken between two points and the work done going backwards is negative of that going forward
  - Conservative force field normally only depends on spatial distances not velocity and acceleration as then the forces change direction when moving backwards and so the second condition won't be met e.g friction
- Examples of conservative force fields are:
  - Gravity
  - Tension/Materials without hystersis
  - Torisonal springs without hystersis
- When a force acts on an object that isn't from a conservative force field you must include work done on one side of the equation
  - Be careful what side you put the work done on.
