# Analysis of Circuits
As long as you stay constant the magnitude of a complex voltage/current can either by peak or rms. Rms is useful in power circuits.

If the current and voltage are sinusoids, then the power will also be. It will have twice the frequency and will have a constant (its equilibrium isn't 0, moved up and down on the y axis)

Power can be negative when current and voltage are opposite. If the power is the energy the supply gives to the load and negative power means the load is giving energy to the supply. As the components we are talking about are passive they must be storing energy then releasing it.

IF you follow the maths you get that the power is equal to:

$$\Large p(t) = \widetilde {V} \widetilde I(cos(2\omega t + \alpha + \beta) + cos(\alpha - \beta))$$

It shows the double the power and the constant term. Since the constatnt is the average power thats what we care about. So average power in ac not only depends on $\widetilde V$ and $\widetilde I$ but also the phase difference between them.

Also due to impedance the current can never be more than 90 degrees out of phase with the voltage so cos will always be positive so avergage power is always positive which makes sense. 
However, it is useful to know which leads and which lags so we can find the phase of the rms values.

IF current lags behind voltage - it is an inductive load
If current leads voltage - it is a capacitive load (due to 1/their impedance)

As the phase of the rms current is relative to the voltage its obvious from ohms law that the total impedence of the load must have the opposite and equal phase

You can resolve current parallel and perp to the voltage. The parallel component is called the active and the perpendicular is the reactive. You can see clearly the active component mulitplied by the voltage gives you the average power so average power is only contributed to by the current that flows in phase with the voltage

That means the power due to reactance is never dissipated and so must be oscillating back and forth - this is done by the load storing energy, this is called reactive power. It has unit volts-amps reactance VAR.

If we look at a ac cicuit with a lone resistor,